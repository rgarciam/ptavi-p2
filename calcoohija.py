# Rebeca Garcia Mencia
# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multiplicacion(self):

        return float(self.operando1) * float(self.operando2)

    def division(self):

        try:

            return float(self.operando1) / float(self.operando2)

        except (ZeroDivisionError, ValueError):

            return "Division by zero is not allowed"

    def Operar(self):

        if self.operador == 'multiplicacion':
            return self.multiplicacion()
        elif self.operador == 'division':
            return self.division()
        else:
            return calcoo.Calculadora.Operar(self)


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("Usage: calculadora.py operando1 operacion operando2")

    operador = sys.argv[2]
    operando1 = sys.argv[1]
    operando2 = sys.argv[3]

    try:

        operando1 = float(operando1)
        operando2 = float(operando2)

    except ValueError:

        sys.exit("Solo se aceptan ints y floats")

    c = CalculadoraHija(operador, operando1, operando2)
    resultado = c.Operar()
    print(resultado)
