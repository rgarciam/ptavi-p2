# Rebeca Garcia Mencia
# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: calcplus.py fichero")

    input = open(sys.argv[1])
    lineas = input.readlines()

    for linea in lineas:
        datos = linea.split(",")
        operador = datos[0]
        resultado = datos[1]

        for operandos in datos[2:]:
            c = calcoohija.CalculadoraHija(operador, resultado, operandos)
            resultado = c.Operar()
        print(resultado)
