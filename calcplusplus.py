# Rebeca Garcia Mencia
# !/usr/bin/python3
# -*- coding: utf-8 -*-

import csv
import sys
import calcoohija

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: calcplusplus.py fichero")

    with open(sys.argv[1], newline="") as lineas:

        fichero = csv.reader(lineas, delimiter=",")

        for datos in fichero:
            operador = datos[0]
            resultado = datos[1]

            for operando in datos[2:]:
                c = calcoohija.CalculadoraHija(operador, resultado, operando)
                resultado = c.Operar()
            print(resultado)
